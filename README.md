# PTB-XL: Visualization of ECG using 2D Vector Loops

![Healthy patient, projection parallel to QRS](./images/Healthy_parallel.png "2D Vector Loop of a healthy patient and Projection parallel to the QRS Loop")

## Interactively Exploration of 6-lead ECG using 2D Vector Loops 

This tool uses the the PTB-XL database because of its rich metadata explained in https://www.nature.com/articles/s41597-020-0495-6. The data itself can be found in https://physionet.org/content/ptb-xl/1.0.2/. The actual (2023-04) version number is 1.0.3.

## Description
The right-hand visualization shows any lead of the 6-lead ECG in time domain. The individual leads can be adressed through their angles in the Cabrera-Sequence. The left-hand visualization shows the 2D vector loop as plot with leads $I$ and $-m\cdot aV\!F$, where $m=2/\sqrt 3$, as x-and y-axes. The menu offers choosing the projection angle of the 2D-VCG and the start and end of the ECG signal. A sample of interest is highlighted and the corresponding polar coordinates (using atan2) are shown. This modification factor ensures, that arbritary angles between the 30°-steps of the Cabrera Sequence can be visualized. This completes the Cabrera-Sequence towards a circle.

## Installation
Install a suitable version of R following the instructions on https://cran.r-project.org/. There you can find the documentation of the available packages (aka libraries) too. If you want to compile packages from source, you need to install RTools https://cran.r-project.org/bin/windows/Rtools/. The GUI compatible with the manipulate package is RStudio, which can be found at https://posit.co/download/rstudio-desktop/. The installation of the WFDB binaries is described in https://www.physionet.org/physiotools/wag/install.htm.

First, you have to install the required libraries "manipulate" and "colorspace". This can be done using the "Tools" menu of RStudio. Then you put the two R scripts of this repository in the base directory of your PTB-XL database. To convert the WFDB-files to ASCII-text, you might have to run "ptbxl2txt.R" once. Only for this, the WFDB binaries have to be installed. The functions for using the interactive plots are stored in "ptbxl.R". 

## Usage
Open RStudio and check your working directory with "getwd()". Use "setwd(path-to-your-PTB-XL-database)" to move to the correct working directory. The run "source("ptbxl.R", encoding="UTF-8") to get acces to the implemented functions and the menu. A record can be selected through its name in the database, e.g. "00553_hr" for high resolution or "00553_lr" for low resolution or its row index in the metadata, which are stored in the dataframe named "ptbxl", e.g. 541. The latter feature can be used to pre-select multiple files from the SCP-codes in the metadata. A report of some important metadata is given by "Report(your-choice)". 

The interactive function to explore the selected 6-lead ECG is called "CabMan", and has typically to be called with the name or row index of the record as an argument. The interaction is activated by clicking on the wheel at the top left of the plot. The red line is the angle of projection (first slider). With the next two sliders, the beginning and the end of a subset of the data can be modified. A sample of interest is highlighted as a black line, it can be modified with the last two sliders, which are divided in a coarse ticker with 0.5 s steps and a finer one to allow to adress the sample within the temporal resolution of the database.

A second interactive plot is called "OrdMan". It shows all signals from the 12-lead ECG, such that the first row gives the 6-lead part in the modified Cabrera sequence and the second row shows the chest leads in their usual order. Here, a sample of interest is highlighted red and the correspondig amplitudes are shown in the respective headers of the plots.

## Support
If you really got stuck, contact "henning.dathe@med.uni-goettingen.de".

## Roadmap
This project was initiated to understand individual ECGs to some depth. The goal to reach is to comprise the chest leads in order to achive a 3D-VCG, which is compatible with the Cabrera Circle.

## Contributing
Before contributing a major update, the maintainer (see Support) would like to be contacted, so that no conflicts with is own work will appear.

## Authors and acknowledgment
First development: henning.dathe@med.uni-goettingen.de

Helpful comments: nicolai.spicher@med.uni-goettingen.de

## License
GPL-3.0 https://www.gnu.org/licenses/gpl-3.0.de.html

## Project status
2023-04-18: First public release, unofficial version number is 0.1
