# This script assumes, that you have WFDB from PhysioNet installed

# Moody, G., Pollard, T., & Moody, B. (2022). 
# WFDB Software Package (version 10.7.0). 
# PhysioNet. https://doi.org/10.13026/gjvw-1m31.

# Goldberger, A., Amaral, L., Glass, L., Hausdorff, J., Ivanov, P. C., Mark, R., ... & Stanley, H. E. (2000). 
# PhysioBank, PhysioToolkit, and PhysioNet: 
# Components of a new research resource for complex physiologic signals. 
# Circulation [Online]. 101 (23), pp. e215–e220. 

# Type in the path in which your PTB-XL database is stored.
setwd("B:/proj/Biosignale/PhysioNet/ptb-xl-a-large-publicly-available-electrocardiography-dataset-1.0.2")

# First, you have to correct the RECORDS-file in line 21799: 
# Add a newline in the middle of "records100/21000/21837_lrrecords500/00000/00001_hr".
records <- read.table("RECORDS")
# Check
head(records)

# Now convert the low-resolution and high-resolution records
for(rec in records$V1){
  cmdchr <- c("-r ", rec)
  outstr <- paste0(rec, ".txt")
  # print(cmdchr)
  # print(outstr)
  system2("rdsamp", args=cmdchr, stdout=outstr)
}



